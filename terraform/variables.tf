variable "region" {
  type = string
}

variable "namespace" {
  type = string
}

variable "kubeconfig" {
  type = string
}

variable "subnets" {
  type = string
}

variable "vpc_id" {
  type = string
}

locals {
  region    = var.region
  namespace = var.namespace
}