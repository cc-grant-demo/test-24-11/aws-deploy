terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.7"
    }

    dns = {
      source  = "hashicorp/dns"
      version = "~> 3.2"
    }
  }
  backend "http" {}
}

provider "aws" {

}

provider "kubernetes" {
  config_path = var.kubeconfig
}