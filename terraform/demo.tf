locals {
  portdemo = 5432
}

resource "aws_db_subnet_group" "subnet-demo" {
  name       = "subnet-demo"
  subnet_ids = split(",", var.subnets)

  tags = {
    Name      = "demo"
    Region    = local.region
    ManagedBy = "Terraform"
  }
}

resource "aws_security_group" "sg-demo" {
  name   = "demo-group"
  vpc_id = var.vpc_id

  ingress {
    from_port   = local.portdemo
    to_port     = local.portdemo
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = local.portdemo
    to_port     = local.portdemo
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name      = "demo"
    Region    = local.region
    ManagedBy = "Terraform"
  }
}

resource "aws_db_instance" "rds-demo" {
  identifier        = "demo"
  instance_class    = "db.t3.small"
  allocated_storage = 15

  engine         = "postgres"
  engine_version = "13.4"

  name     = "demo"
  username = "demo"
  password = "demopassword"
  port     = local.portdemo

  db_subnet_group_name   = aws_db_subnet_group.subnet-demo.name
  vpc_security_group_ids = [aws_security_group.sg-demo.id]
  publicly_accessible    = true
  skip_final_snapshot    = true
}

data "dns_a_record_set" "dns-resolver-demo" {
  host = aws_db_instance.rds-demo.address
}

resource "kubernetes_service" "service-demo" {
  metadata {
    name      = "service-demo"
    namespace = local.namespace
  }

  spec {
    cluster_ip = "None"
    port {
      port        = local.portdemo
      target_port = local.portdemo
      protocol    = "TCP"
    }
  }
}

resource "kubernetes_endpoints" "endpoint-demo" {
  metadata {
    name      = "endpoint-demo"
    namespace = local.namespace
  }

  subset {
    address {
      ip = data.dns_a_record_set.dns-resolver-demo.addrs[0]
    }

    port {
      name     = "service-demo"
      port     = local.portdemo
      protocol = "TCP"
    }
  }
}